type CreateCustomerRequest = {
    firstName: string
    lastName: string
    email: string
    preferenceIds: string[]
}