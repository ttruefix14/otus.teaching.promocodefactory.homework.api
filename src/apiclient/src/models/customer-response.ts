type CustomerResponse = {
    id: string
    firstName: string
    lastName: string
    email: string
    preferences: PreferenceResponse[]
}