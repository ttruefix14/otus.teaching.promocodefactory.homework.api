type UpdateCustomerRequest = {
    id: string
    firstName: string
    lastName: string
    email: string
    preferenceIds: string[]
}