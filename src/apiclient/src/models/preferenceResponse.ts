type PreferenceResponse = {
    id: string
    name: string
}