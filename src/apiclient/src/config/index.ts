export const API_URL = import.meta.env.VITE_REACT_API_URL as string;
export const SIGNALR_URL = import.meta.env.VITE_REACT_SIGNALR_URL as string;