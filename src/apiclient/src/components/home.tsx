import { useEffect, useState } from "react";
import LoginForm from "./login-form";
import CustomerService from "./customer-service";

const Home = () => {
    const [token, setToken] = useState<string>();

    useEffect(() => {
        let token_ = window.localStorage.getItem("token")
        if (token_) setToken(token_)
    }, []);

    const clearToken = () => {
        setToken(undefined)
        window.localStorage.removeItem("token")
    }

    return <div className="home">
        <h1>Домашняя страница</h1>
        {token ? <div>
            <p>Время жизни токена по умолчанию - 30 минут</p>
            <button onClick={clearToken}>Очистить токен</button>
            <CustomerService></CustomerService>
        </div>
            : <div>
                <h2>Авторизация</h2>
                <LoginForm setToken={setToken}></LoginForm>
            </div>}
    </div>
}

export default Home