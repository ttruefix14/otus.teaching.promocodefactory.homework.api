import { HubConnection, HubConnectionBuilder, HubConnectionState } from "@microsoft/signalr";
import { useEffect, useRef, useState } from "react";
import { API_URL, SIGNALR_URL } from "../config";
import CustomerCard from "./customer-card";
import CreateCustomerForm from "./create-customer-form";

const CustomerService = () => {
    const [connection, setConnection] = useState<HubConnection | null>()
    const [customers, setCustomers] = useState<Map<string, CustomerResponse>>(new Map<string, CustomerResponse>())
    const currentCustomers = useRef<Map<string, CustomerResponse>>(customers)
    currentCustomers.current = customers

    useEffect(() => {
        const newConnection = new HubConnectionBuilder()
            .withUrl(`${API_URL}/customers`,
                {
                    accessTokenFactory: () => window.localStorage.getItem("token") ?? "",
                })
            .withAutomaticReconnect() // Если отвалилось соединение - пытаемся еще раз
            .build()

        setConnection(newConnection);


        if (newConnection) {
            newConnection.start()
                .then(() => {
                    console.log('connected signalR!')

                    newConnection.on('SendCustomers', (message: CustomerResponse[]) => {
                        console.log('customers received', message)
                        setCustomers(new Map(message.map(c => [c.id, c])))
                    });

                    newConnection.on('SendCustomer', (message: CustomerResponse) => {
                        console.log('customer received', message)
                        const updatedCustomers = new Map([...currentCustomers.current]);
                        updatedCustomers.set(message.id, message)
                        setCustomers(updatedCustomers)
                    });

                    newConnection.on('SendUpdatedCustomer', (message: CustomerResponse) => {
                        console.log('updated customer received', message)
                        const updatedCustomers = new Map([...currentCustomers.current]);
                        updatedCustomers.set(message.id, message)
                        setCustomers(updatedCustomers)
                    });

                    newConnection.on('SendDeletedCustomerId', (message: string) => {
                        console.log('delete customer id received', message)
                        const updatedCustomers = new Map([...currentCustomers.current]);
                        updatedCustomers.delete(message)
                        setCustomers(updatedCustomers)
                    });


                    if (customers.keys.length === 0)
                        newConnection.send('GetCustomers')
                })
                .catch(e => console.log('Ошибка подключения: ', e))
        }


        return () => {
            if (newConnection) {
                newConnection.stop()
            }
        }
    }, [])

    const createCustomer = (customer: CreateCustomerRequest) => {
        connection!.send("CreateCustomer", customer)
    }

    const updateCustomer = (customer: UpdateCustomerRequest) => {
        connection!.send("UpdateCustomer", customer)
    }

    const deleteCustomer = (id: string) => {
        connection!.send("DeleteCustomer", id)
    }

    return <div className="customer-service">
        <h2>Сервис управления клиентами</h2>
        <span>При обновлении страницы подгружается все клиенты.</span>
        <span>При добавлении, обновлении, удалении клиентов - информация обновляется</span>
        <CreateCustomerForm createCustomer={createCustomer}></CreateCustomerForm>
        {Array.from(customers, (v => v)).map((value, index) => {
            return <CustomerCard key={index} customer={value[1]} updateCustomer={updateCustomer} deleteCustomer={deleteCustomer}></CustomerCard>
        })}
    </div>
}

export default CustomerService

