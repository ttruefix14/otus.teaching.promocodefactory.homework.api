import { useEffect, useState } from "react"
import { SubmitHandler, useFieldArray, useForm } from "react-hook-form"

type UpdateCustomerForm = {
    firstName: string
    lastName: string
    email: string
    preferences: PreferenceResponse[]
}

type PreferenceId = {
    value: string
}

const CustomerCard = ({ customer, updateCustomer, deleteCustomer }: { customer: CustomerResponse, updateCustomer: any, deleteCustomer: any }) => {
    const [isUpdating, setUpdating] = useState<boolean>(false);

    const {
        register,
        handleSubmit,
        getValues,
        setValue,
        reset,
        control,
        formState: { errors },
    } = useForm<UpdateCustomerForm>({
        defaultValues: {
            firstName: customer.firstName,
            lastName: customer.lastName,
            email: customer.email,
            preferences: customer.preferences
        }
    })

    const { fields, append, remove } = useFieldArray<UpdateCustomerForm>(
        {
            control,
            name: "preferences"
        }
    );

    useEffect(() => {
        setUpdating(false)
        setValue("firstName", customer.firstName)
        setValue("lastName", customer.lastName)
        setValue("email", customer.email)
        setValue("preferences", customer.preferences)
    }, [customer])

    const onSubmit: SubmitHandler<UpdateCustomerForm> = (data) => {
        let form = getValues()
        updateCustomer({ id: customer.id, firstName: form.firstName, lastName: form.lastName, email: form.email, preferenceIds: form.preferences.map(v => v.id) })
    }

    return <div className="customer-card">
        <p><b>{customer.id}</b></p>
        <button onClick={() => setUpdating(!isUpdating)} style={{borderStyle: isUpdating ? "inset" : undefined}}>Редактировать</button>
        <button onClick={() => deleteCustomer(customer.id)}>Удалить</button>
        <form onSubmit={handleSubmit(onSubmit)}>
            <label>Имя</label>
            <input {...register("firstName", { required: true })} disabled={!isUpdating} />
            {errors.firstName ? <span>Это поле обязательное</span> : <br></br>}

            <label>Фамилия</label>
            <input {...register("lastName", { required: true })} disabled={!isUpdating} />
            {errors.lastName ? <span>Это поле обязательное</span> : <br></br>}

            <label>Электронная почта</label>
            <input {...register("email", { required: true })} disabled={!isUpdating} />
            {errors.email ? <span>Это поле обязательное</span> : <br></br>}

            <label>Предпочтения</label>
            <ul className="list">
                {fields.map((item, index) => (
                    <li key={index}>
                        <label>{item.name}</label><br></br>
                        <input
                            key={item.id}
                            {...register(`preferences.${index}.id`)}
                            disabled={!isUpdating} />
                        <button onClick={() => remove(index)} disabled={!isUpdating}>Удалить</button>
                    </li>
                ))}
                <button onClick={() => append({ id: "", name: "" })} disabled={!isUpdating}>Добавить</button><br></br>
            </ul>

            <input type="submit" disabled={!isUpdating}/>
        </form>
    </div>
}

export default CustomerCard