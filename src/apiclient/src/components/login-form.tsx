import { SubmitHandler, useForm } from "react-hook-form"
import { axios } from "../lib/axios"

type Inputs = {
    login: string
}

const LoginForm = ({setToken}:any) => {
    const {
        register,
        handleSubmit,
        watch,
        reset,
        formState: { errors },
    } = useForm<Inputs>()
    const onSubmit: SubmitHandler<Inputs> = (data) => login()

    const login = () => {
        axios.post("/login", { login: watch("login") })
        .then(response => {
            window.localStorage.setItem("token", response.data)
            setToken(response.data)
            reset()
        })
    }

    return (
        /* "handleSubmit" will validate your inputs before invoking "onSubmit" */
        <form onSubmit={handleSubmit(onSubmit)}>
            <label>Имя пользователя</label>
            {/* include validation with required or other standard HTML validation rules */}
            <input {...register("login", { required: true })} />
            {/* errors will return when field validation fails  */}
            {errors.login ? <span>Это поле обязательное</span> : <br></br>}

            <input type="submit" />
        </form>
    )
}

export default LoginForm