import { FieldArrayPath, SubmitHandler, useFieldArray, useForm } from "react-hook-form"

type CreateCustomerForm = {
    firstName: string
    lastName: string
    email: string
    preferenceIds: PreferenceId[]
}

type PreferenceId = {
    value: string
}

const CreateCustomerForm = ({ createCustomer }: any) => {
    const {
        register,
        handleSubmit,
        getValues,
        reset,
        control,
        formState: { errors },
    } = useForm<CreateCustomerForm>()

    const { fields, append, remove } = useFieldArray<CreateCustomerForm>(
        {
            control,
            name: "preferenceIds"
        }
    );

    const onSubmit: SubmitHandler<CreateCustomerForm> = (data) => {
        let form = getValues()
        createCustomer({ firstName: form.firstName, lastName: form.lastName, email: form.email, preferenceIds: form.preferenceIds.map(v => v.value) })
    }

    return (<div className="customer-card add">
        <h3>Добавить клиента</h3>
        <form onSubmit={handleSubmit(onSubmit)}>
            <label>Имя</label>
            <input {...register("firstName", { required: true })} />
            {errors.firstName ? <span>Это поле обязательное</span> : <br></br>}

            <label>Фамилия</label>
            <input {...register("lastName", { required: true })} />
            {errors.lastName ? <span>Это поле обязательное</span> : <br></br>}

            <label>Электронная почта</label>
            <input {...register("email", { required: true })} />
            {errors.email ? <span>Это поле обязательное</span> : <br></br>}

            <label>Предпочтения</label>
            <ul className="list">
                {fields.map((item, index) => (
                    <li key={index}>
                        <input
                            key={item.id}
                            {...register(`preferenceIds.${index}.value`)}
                        />
                        <button onClick={() => remove(index)}>Удалить</button>
                    </li>
                ))}
                <button onClick={() => append({ value: "" })}>Добавить</button><br></br>
            </ul>

            <input type="submit" />
        </form>
    </div>
    )
}

export default CreateCustomerForm