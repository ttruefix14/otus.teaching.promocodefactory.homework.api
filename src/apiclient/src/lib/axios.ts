import Axios from "axios";
import { API_URL } from "../config";

export const axios = Axios.create({
  baseURL: API_URL,
});

// axios.interceptors.request.use(
//   (config) => {
//     const token = window.localStorage.getItem("token")
//     if (token) {
//       config.headers['Authorization'] = `Bearer ${token}`
//     }
//     return config;
//   },
//   (error) => {
//     Promise.reject(error)
//   }
// );