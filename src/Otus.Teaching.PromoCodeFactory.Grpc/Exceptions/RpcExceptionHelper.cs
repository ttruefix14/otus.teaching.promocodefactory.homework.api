﻿using Grpc.Core;
using System;
using System.Collections.Generic;
using static Grpc.Core.Metadata;

namespace Otus.Teaching.PromoCodeFactory.Grpc.Exceptions
{
    public static class RpcExceptionHelper
    {
        public static void ThrowInvalidArgumentGuidException(string guidString)
        {
            throw new RpcException(new Status(StatusCode.InvalidArgument, $"Value: \"{guidString}\" is not guid"));
        }

        public static void ThrowInvalidArgumentGuidException(List<string> guidStrings)
        {
            throw new RpcException(new Status(
                StatusCode.InvalidArgument, $"Value: \"{string.Join(", ", guidStrings)}\" are not guids"));
        }

        public static void ThrowNotFoundException(Guid id)
        {
            throw new RpcException(new Status(StatusCode.NotFound, $"Entity with id: \"{id}\" not found"));
        }

        public static void ThrowNotFoundException(List<Guid> ids)
        {
            throw new RpcException(new Status(StatusCode.NotFound, $"Entities with ids: \"{string.Join(", ", ids)}\" not found"));
        }
    }
}
