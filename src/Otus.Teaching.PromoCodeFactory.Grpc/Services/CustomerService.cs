﻿using Google.Protobuf.WellKnownTypes;
using Grpc.Core;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.Grpc.Exceptions;
using Otus.Teaching.PromoCodeFactory.Grpc.Extensions;
using System;
using System.Linq;
using System.Threading.Tasks;
using static Otus.Teaching.PromoCodeFactory.Grpc.CustomerResponse.Types;
using static Otus.Teaching.PromoCodeFactory.Grpc.Customers;
using static Otus.Teaching.PromoCodeFactory.Grpc.CustomersResponse.Types;

namespace Otus.Teaching.PromoCodeFactory.Grpc.Services
{
    public class CustomerService : CustomersBase
    {
        private readonly IRepository<Customer> _customerRepository;
        private readonly IRepository<Preference> _preferenceRepository;

        public CustomerService(IRepository<Customer> customerRepository, IRepository<Preference> preferenceRepository)
        {
            _customerRepository = customerRepository;
            _preferenceRepository = preferenceRepository;
        }

        public override async Task<CustomersResponse> GetCustomers(Empty request, ServerCallContext context)
        {
            var customers = await _customerRepository.GetAllAsync();

            var response = new CustomersResponse();
            response.Customers.AddRange(customers.Select(x => new CustomerShortResponse
            {
                Id = x.Id.ToString(),
                Email = x.Email,
                FirstName = x.FirstName,
                LastName = x.LastName
            }));

            return response;
        }

        public override async Task<CustomerResponse> GetCustomer(CustomerRequest request, ServerCallContext context)
        {
            Guid id = request.Id.ToGuid();
            var customer = await _customerRepository.GetByIdAsync(id);
            if (customer == null)
                RpcExceptionHelper.ThrowNotFoundException(id);

            var response = new CustomerResponse
            {
                Id = customer.Id.ToString(),
                FirstName = customer.FirstName,
                LastName = customer.LastName,
                Email = customer.Email,
            };

            response.Preferences.AddRange(customer.Preferences.Select(x => new PreferenceResponse
            {
                Id = x.Preference.Id.ToString(),
                Name = x.Preference.Name
            }).ToList());

            return response;
        }

        public override async Task<Empty> CreateCustomer(CreateCustomerRequest request, ServerCallContext context)
        {
            Guid[] ids = request.PreferenceIds.ToArray().ToGuids();

            var preferences = await _preferenceRepository.GetRangeByIdsAsync(ids.ToList());

            if (preferences.Count() != ids.Length)
                RpcExceptionHelper.ThrowNotFoundException(
                    ids.Where(x => !preferences.Select(x => x.Id).Contains(x)).ToList());

            Customer customer = new Customer
            {
                FirstName = request.FirstName,
                LastName = request.LastName,
                Email = request.Email,
                Preferences = preferences.Select(x => new CustomerPreference { Preference = x }).ToArray()
            };


            await _customerRepository.AddAsync(customer);

            return new Empty();
        }
        public override async Task<Empty> UpdateCustomer(UpdateCustomerRequest request, ServerCallContext context)
        {
            Guid id = request.Id.ToGuid();
            Guid[] preferenceIds = request.PreferenceIds.ToArray().ToGuids();

            var customer = await _customerRepository.GetByIdAsync(id);
            if (customer == null)
                RpcExceptionHelper.ThrowNotFoundException(id);

            var preferences = await _preferenceRepository.GetRangeByIdsAsync(preferenceIds.ToList());
            if (preferences.Count() != preferenceIds.Length)
                RpcExceptionHelper.ThrowNotFoundException(
                    preferenceIds.Where(x => !preferences.Select(x => x.Id).Contains(x)).ToList());

            customer.FirstName = request.FirstName;
            customer.LastName = request.LastName;
            customer.Email = request.Email;

            customer.Preferences.Clear();
            customer.Preferences = preferences.Select(x => new CustomerPreference()
            {
                Preference = x,
            }).ToList();

            await _customerRepository.UpdateAsync(customer);

            return new Empty(); 
        }

        public override async Task<Empty> DeleteCustomer(CustomerRequest request, ServerCallContext context)
        {
            Guid id = request.Id.ToGuid();
            var customer = await _customerRepository.GetByIdAsync(id);

            if (customer == null)
                RpcExceptionHelper.ThrowNotFoundException(id);

            await _customerRepository.DeleteAsync(customer);

            return new Empty();
        }
    }
}