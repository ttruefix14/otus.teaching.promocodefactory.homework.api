﻿using Otus.Teaching.PromoCodeFactory.Grpc.Exceptions;
using System;
using System.Collections.Generic;

namespace Otus.Teaching.PromoCodeFactory.Grpc.Extensions
{
    public static class StringExtensions
    {
        public static Guid ToGuid(this string guidString)
        {
            Guid id;
            if (!Guid.TryParse(guidString, out id))
                RpcExceptionHelper.ThrowInvalidArgumentGuidException(guidString);
            return id;
        }

        public static Guid[] ToGuids(this string[] guidStrings)
        {
            Guid[] ids = new Guid[guidStrings.Length];
            List<string> badGuids = new List<string>();
            for (int i = 0; i < guidStrings.Length; i++)
            {
                if (!Guid.TryParse(guidStrings[i], out ids[i]))
                    badGuids.Add(guidStrings[i]);
            }
            if (badGuids.Count > 0)
                RpcExceptionHelper.ThrowInvalidArgumentGuidException(badGuids);
            return ids;
        }
    }
}
