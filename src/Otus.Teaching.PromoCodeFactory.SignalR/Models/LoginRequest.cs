﻿namespace Otus.Teaching.PromoCodeFactory.SignalR.Models
{
    public class LoginRequest
    {
        public string Login {  get; set; }
    }
}
