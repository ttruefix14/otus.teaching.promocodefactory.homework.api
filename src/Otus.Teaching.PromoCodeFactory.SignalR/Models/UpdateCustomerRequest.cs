﻿using System;
using System.Collections.Generic;

namespace Otus.Teaching.PromoCodeFactory.SignalR.Models
{
    public class UpdateCustomerRequest
    {
        public Guid Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public List<Guid> PreferenceIds { get; set; }
    }
}