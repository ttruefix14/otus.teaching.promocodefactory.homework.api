﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.SignalR.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Otus.Teaching.PromoCodeFactory.SignalR.Mappers
{
    public class CustomerMapper
    {
        public static Customer MapFromModel(UpdateCustomerRequest model, IEnumerable<Preference> preferences, Customer customer)
        {
            customer.FirstName = model.FirstName;
            customer.LastName = model.LastName;
            customer.Email = model.Email;

            customer.Preferences.Clear();
            customer.Preferences = preferences.Select(x => new CustomerPreference()
            {
                Preference = x,
            }).ToList();

            return customer;
        }

        public static Customer MapFromModel(CreateCustomerRequest model, IEnumerable<Preference> preferences)
        {
            return new Customer
            {
                Id = Guid.NewGuid(),
                FirstName = model.FirstName,
                LastName = model.LastName,
                Email = model.Email,
                Preferences = preferences.Select(x => new CustomerPreference()
                {
                    Preference = x
                }).ToList()
            };
        }
    }
}
