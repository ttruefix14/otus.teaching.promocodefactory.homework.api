﻿using Otus.Teaching.PromoCodeFactory.SignalR.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.SignalR.Hubs.Abstractions
{
    public interface ICustomersServiceHub
    {
        Task SendCustomers(List<CustomerResponse> response);

        Task SendCustomer(CustomerResponse response);

        Task SendUpdatedCustomer(CustomerResponse response);

        Task SendDeletedCustomerId(Guid id);
    }
}
