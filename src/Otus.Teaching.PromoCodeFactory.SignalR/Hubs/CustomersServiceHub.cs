﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.SignalR;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.SignalR.Hubs.Abstractions;
using Otus.Teaching.PromoCodeFactory.SignalR.Mappers;
using Otus.Teaching.PromoCodeFactory.SignalR.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.SignalR.Hubs
{
    [Authorize]
    public class CustomersServiceHub : Hub<ICustomersServiceHub>
    {
        private readonly IRepository<Customer> _customerRepository;
        private readonly IRepository<Preference> _preferenceRepository;

        public CustomersServiceHub(IRepository<Customer> customerRepository, IRepository<Preference> preferenceRepository)
        {
            _customerRepository = customerRepository;
            _preferenceRepository = preferenceRepository;
        }
        
        public async Task GetCustomers()
        {
            var customers = await _customerRepository.GetAllAsync();

            var response = customers.Select(x => new CustomerResponse(x)).ToList();

            await Clients.Caller.SendCustomers(response);
        }

        public async Task CreateCustomer(CreateCustomerRequest request)
        {
            var preferences = await _preferenceRepository.GetRangeByIdsAsync(request.PreferenceIds);

            Customer customer = CustomerMapper.MapFromModel(request, preferences);

            await _customerRepository.AddAsync(customer);

            await Clients.All.SendCustomer(new CustomerResponse(customer));
        }

        public async Task UpdateCustomer(UpdateCustomerRequest request)
        {
            var customer = await _customerRepository.GetByIdAsync(request.Id);

            if (customer == null)
                throw new KeyNotFoundException(request.Id.ToString());

            var preferences = await _preferenceRepository.GetRangeByIdsAsync(request.PreferenceIds);

            customer = CustomerMapper.MapFromModel(request, preferences, customer);

            await _customerRepository.UpdateAsync(customer);

            await Clients.All.SendUpdatedCustomer(new CustomerResponse(customer));
        }

        public async Task DeleteCustomer(Guid id)
        {
            var customer = await _customerRepository.GetByIdAsync(id);

            if (customer == null)
                throw new KeyNotFoundException(id.ToString());

            await _customerRepository.DeleteAsync(customer);

            await Clients.All.SendDeletedCustomerId(id);
        }
    }
}
