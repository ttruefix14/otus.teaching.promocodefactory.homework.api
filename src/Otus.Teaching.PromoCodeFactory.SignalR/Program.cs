using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using Otus.Teaching.PromoCodeFactory.DataAccess;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;
using Otus.Teaching.PromoCodeFactory.SignalR.Hubs;
using Otus.Teaching.PromoCodeFactory.SignalR.Models;
using Otus.Teaching.PromoCodeFactory.SignalR.Options;
using Otus.Teaching.PromoCodeFactory.SignalR.UserManager;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

var builder = WebApplication.CreateBuilder(args);


// Add services to the container.
builder.Services.AddInfrastructureServices("Filename=../Otus.Teaching.PromoCodeFactory.DataAccess/PromoCodeFactoryDb.sqlite");

builder.Services.AddSignalR();
builder.Services.AddSingleton<IUserIdProvider, UserProvider>();

var authOptions = builder.Configuration.Get<AuthOptions>();
builder.Services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(options =>
                {
                    options.RequireHttpsMetadata = false;
                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuer = true,
                        ValidIssuer = authOptions.Issuer,
                        ValidateAudience = true,
                        ValidAudience = authOptions.Audience,
                        ValidateLifetime = true,
                        IssuerSigningKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(authOptions.SecurityKey)),
                        ValidateIssuerSigningKey = true,
                    };

                    options.Events = new JwtBearerEvents
                    {
                        OnMessageReceived = context =>
                        {
                            var accessToken = context.Request.Query["access_token"];

                            // If the request is for our hub...
                            var path = context.HttpContext.Request.Path;
                            if (!string.IsNullOrEmpty(accessToken) &&
                                (path.StartsWithSegments("/customers")))
                            {
                                // Read the token out of the query string
                                context.Token = accessToken;
                            }

                            return Task.CompletedTask;
                        }
                    };
                });

builder.Services.AddCors(options =>
{
    options.AddDefaultPolicy(policy =>
    {
        policy.WithOrigins("http://localhost:5173")
            .AllowAnyHeader()
            .AllowCredentials()
            .AllowAnyMethod();

    });
});

var app = builder.Build();

// Configure the HTTP request pipeline.

app.UseHttpsRedirection();
app.UseCors();
app.UseRouting();
app.UseAuthentication();
app.UseAuthorization();

app.MapGet("/", () =>
{
    return "������ ������ � ��������� �� SignalR";
});

app.MapPost("/login", ([FromBody] LoginRequest request) =>
{
    var claims = new List<Claim> {
                new Claim(ClaimsIdentity.DefaultNameClaimType, request.Login),
                new Claim(ClaimsIdentity.DefaultRoleClaimType, request.Login)
             };

    var claimsIdentity = new ClaimsIdentity(claims, "Token", ClaimsIdentity.DefaultNameClaimType, ClaimsIdentity.DefaultRoleClaimType);


    var now = DateTime.UtcNow;
    // ������� JWT-�����
    var jwt = new JwtSecurityToken(
        issuer: authOptions.Issuer,
                audience: authOptions.Audience,
        notBefore: now,
        claims: claimsIdentity.Claims,
                expires: now.Add(TimeSpan.FromMinutes(authOptions.Lifetime)),
                signingCredentials: new SigningCredentials(new SymmetricSecurityKey(Encoding.ASCII.GetBytes(authOptions.SecurityKey)), SecurityAlgorithms.HmacSha256));
    var encodedJwt = new JwtSecurityTokenHandler().WriteToken(jwt);

    return encodedJwt;
});

app.MapHub<CustomersServiceHub>("/customers");

app.Services.CreateScope().ServiceProvider.GetService<IDbInitializer>().InitializeDb();
app.Run();