﻿using Microsoft.AspNetCore.SignalR;

namespace Otus.Teaching.PromoCodeFactory.SignalR.UserManager
{
    public class UserProvider : IUserIdProvider
    {
        public string GetUserId(HubConnectionContext connection)
        {
            return connection.GetHttpContext().User?.Identity?.Name;
        }
    }
}
